# README #

The IRIS (Intelligent Routing and Informational Signage) product is a navigation app that routes workers to a destination, allowing any worker to deliver orders to customers. IRIS has been designed to be easy to use for any user, even those with various disabilities including vision impairment.

## Features ##
* IRIS features rich support for accessibility features including full VoiceOver support, Dynamic Type support, Bold Text support, Reduced Transparency support, etc.
* IRIS provides voice announcements and allows the user to customize their audible experience, including customizing the voice pitch and rate, and they may enable continuous voice prompts and specify how frequently the announcements will occur.
* Upon logging into the app, IRIS presents the list of pending orders fetched from the database. Once the user taps an order, the orders screen is dismissed and IRIS begins to load a route to the destination. Once the user arrives at the destination, a toolbar will appear and provide a button that allows the user to mark the order as delivered.
* Four buttons are provided at the bottom of the navigation screen that provide all of the navigation necessary to take full advantage of the app, including from left to right Settings, Order Details, Orders, and Help. Help provides buttons for the user to contact someone for help or navigate back to the store.

## Useful Tips ##
* A user account with username 'default' and password 'abc123' has been set up to allow logging into the app.
* A lot of the app functionality can be tested on the iOS Simulator, besides the actual navigation of course.
* IRIS will fail to load a route if there are no nodes around it. At this time the only nodes available are placed around the WSU campus. You can change the location of the iOS Simulator via Debug > Location > Custom Location - provide a lat and long that lies on WSU's campus in order to load a route.
* If Google Maps complains you're running an outdated version of the maps SDK, the newest version can be downloaded and placed into the project to replace the old framework. We do not envision problems arising by running the latest version available.
* The iPhone's silence switch must not be activated in order to hear the voice announcements.

## Other Info ##
* IRIS is compatible with iPhones running iOS 8 and higher.
* IRIS uses Google Maps for routing because it provides some data other mapping services don't have yet, including paths around WSU and indoor data for the different floor levels. The project could be reworked to utilize a different mapping service if desired.
* IRIS pulls data from a database in order to fetch orders and order details, and uses a custom node database in order to calculate the quickest path to the destination.
* IRIS generates its own turn-by-turn directions; it does not rely on any third-party service for walking directions.

## How do I get set up? ##

1. Download the project
2. Open the .xcodeproj file in Xcode 6+
3. Open the GoogleMapsAPI.m file and follow the instructions to obtain API keys
4. Run the app on the iOS Simulator or a physical device running iOS 8.0+ (developer account required for testing on real device)

## How is the project organized? ##
* IRIS follows the MVC paradigm and its organization reflects this pattern. 
* Controllers are found at the root level, along with the main view (storyboard), launch screen, and images. Also found at root level is the Google Maps API key class and the AppDelegate.
* Interface views, besides the storyboard, are located in the Views directory.
* A class that contains URLs and credentials for the ordering API is provided in the OrdersAPI directory.
* The custom node database API lies within the API Objects directory. This is the API Klint Holmes had created for us. The APISettings class in this directory provides URLs and credentials to access the custom node database.
* The AFNetworking directory and associated category provides an easy to use third-party API to make network requests.

## How does it work? ##
* Most of the operations performed throughout the app are fairly standard for iOS development - the verbose code is quite readable. The real magic happens in the mapping navigation view controller. Once the user selects an order, they are unwinded back to the navigation controller at which point Core Location kicks in and asks the user for permission to access their location. Once the user grants permission, a delegate method is called and the location manager will begin updating the user's location. Once the location is accurate enough, it will fetch a route from the database and proceed to draw it. Once it is drawn, navigation and voice announcements begin. Every time the device's heading updates, the camera is updated to always indicate the direction the user is traveling. Every time the location is updated, various calculations are performed such as calculating the estimated arrival time, distance remaining, upcoming directions, etc. This is where the nodes are removed as the user reaches them, and the next set of directions are calculated.

## Where to go from here? ##
* Some aspects of the project will need to be QA'd and tested further in real-time, including audible announcements especially continuous prompts to ensure the timing and information is delivered appropriately, and the process involved once an order is marked as delivered.
* To improve the app, functionality should be added to direct the user to the first node based on the device heading, and reroute if they navigate too far from the route.
* The ultimate goal of IRIS is to provide directions from any given location to any possible destination, which includes both outdoor and indoor navigation. Nothing has been implemented in regards to indoor navigation once the GPS signal is lost. We did research and prototype the use of iBeacon technology in order to obtain the user's location within the building and provide directions using the same routing system. Additionally, we had imagined an interactive directory assistance (IS of IRIS) that would allow the user to interact with a directory of information using voice commands and it would speak information back to the user, similar to Siri. This would be implemented using iBeacon technology as well, so when the user is within range of the directory location the interface would present the ability to enter that voice interaction mode.
* Other nifty features we had thought of but didn't have the time or resources to implement include: ability to tap directions at the top to see an overview that lists all of the upcoming directions, ability for manager to view orders for more than the current day as well as orders already picked up/delivered, ability for manager to view their employee's current position on a map, ability for customer to see where their sandwich is or get text message when they're close.

## Who developed IRIS, who can I reach out to with questions? ##

* Project Manager: Rich Fry - rich@richfry.com
* Senior Developer: Jordan - hipwelljo@yahoo.com
* Developer: Jeff - jwenz723@gmail.com
* Developer: Zach - zach@zthurston.com
* Team Member: James - jhgreen.77@gmail.com
* Team Member: Aaron - aaronday91@gmail.com
* Team Member: Daniel - DanielEPrescott@gmail.com
* Director at PARC (People with Disabilities Contributing to their Community) - Ken Naegle - knaegle@dsdmail.net, 801-402-0950
* Sodexo (on-campus food delivery service) - https://weber.sodexomyway.com
* Developer of custom node mapping database: Klint Holmes - klintholmes@weber.edu