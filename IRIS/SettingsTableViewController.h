//
//  SettingsTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 3/23/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The Settings table view controller, allowing users to configure app settings
 */
@interface SettingsTableViewController : UITableViewController

@end
