//
//  OrderDetailsTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 2/1/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "OrderDetailsTableViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import "ItemDetailsTableViewController.h"
#import "UIKit+AFNetworking.h"

@interface OrderDetailsTableViewController ()

@property (weak, nonatomic) IBOutlet UIView *tableHeaderView;
@property (weak, nonatomic) IBOutlet UIImageView *profilePhotoImageView;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *phoneNumberButton;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *placedAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *pickedUpAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveredAtLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationHeadingLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemsInOrderLabel;
@property (weak, nonatomic) IBOutlet GMSMapView *locationMapView;

@end


@implementation OrderDetailsTableViewController

static NSString* const CellIdentifier = @"OrderDetailsCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.frame;
        self.tableView.backgroundView = blurEffectView;
        self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    } else {
        self.view.backgroundColor = [UIColor whiteColor];
    }
    
    //Fill in with order details
    
    NSDictionary *orderInfo = self.orderDetails[@"order_details"];
    NSDictionary *locationInfo = self.orderDetails[@"order_location_details"][@"location_data"];
    
    if (orderInfo[@"OrderSummaryCustomerPhoto"] != [NSNull null]) {
        NSString *imageURLString = [NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryCustomerPhoto"]];
        NSURL *imageURL = [NSURL URLWithString:imageURLString];
        [self.profilePhotoImageView setImageWithURL:imageURL placeholderImage:[UIImage imageNamed:@"profile"]];
        self.profilePhotoImageView.accessibilityLabel = @"Customer photo";
        self.profilePhotoImageView.layer.cornerRadius = self.profilePhotoImageView.frame.size.width / 2;
    } else {
        self.profilePhotoImageView.accessibilityLabel = @"Customer photo, blank";
        self.profilePhotoImageView.image = [UIImage imageNamed:@"profile"];
    }
    self.profilePhotoImageView.tintColor = [UIColor lightGrayColor];
    
    self.customerNameLabel.text = [NSString stringWithFormat:@"%@ %@", orderInfo[@"OrderSummaryFirstName"], orderInfo[@"OrderSummaryLastName"]];
    
    if (orderInfo[@"OrderSummaryPhone"] != [NSNull null]) {
        NSString *phoneNumber = [NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryPhone"]];
        [self.phoneNumberButton setTitle:phoneNumber forState:UIControlStateNormal];
        [self.phoneNumberButton setTitle:phoneNumber forState:UIControlStateNormal];
        [self.phoneNumberButton addTarget:self action:@selector(didTapPhone:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        self.phoneNumberButton.hidden = YES;
    }
    
    NSDateFormatter *originalFormatter = [[NSDateFormatter alloc] init];
    originalFormatter.dateFormat = @"yyyy-MM-dd H:m:s";
    NSDateFormatter *shortDateFormatter = [[NSDateFormatter alloc] init];
    shortDateFormatter.dateStyle = NSDateFormatterNoStyle;
    shortDateFormatter.timeStyle = NSDateFormatterShortStyle;
    
    NSString *orderNumber = [NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryTicketNumber"]];
    self.orderNumberLabel.attributedText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"Order #%@", orderNumber] attributes:nil];
    
    NSDate *placedAtDate = [originalFormatter dateFromString:[NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryCreatedOn"][@"date"]]];
    self.placedAtLabel.text = [NSString stringWithFormat:@"Placed at %@", [shortDateFormatter stringFromDate:placedAtDate]];
    
    if (orderInfo[@"OrderSummaryOrderPickedUp"] != [NSNull null]) {
        NSDate *pickedUpAtDate = [originalFormatter dateFromString:[NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryOrderPickedUp"][@"date"]]];
        self.pickedUpAtLabel.text = [NSString stringWithFormat:@"Picked Up at %@", [shortDateFormatter stringFromDate:pickedUpAtDate]];
    } else {
        self.pickedUpAtLabel.text = @"Not Picked Up Yet";
    }
    
    if (orderInfo[@"OrderSummaryOrderDelivered"] != [NSNull null]) {
        NSDate *pickedUpAtDate = [originalFormatter dateFromString:[NSString stringWithFormat:@"%@", orderInfo[@"OrderSummaryOrderDelivered"][@"date"]]];
        self.deliveredAtLabel.text = [NSString stringWithFormat:@"Delivered at %@", [shortDateFormatter stringFromDate:pickedUpAtDate]];
    } else {
        self.deliveredAtLabel.text = @"Not Delivered Yet";
    }
    
    self.locationHeadingLabel.accessibilityTraits = UIAccessibilityTraitHeader;
    self.itemsInOrderLabel.accessibilityTraits = UIAccessibilityTraitHeader;
    
    //set up location map
    CLLocationDegrees lat = [locationInfo[@"lat"] floatValue];
    CLLocationDegrees lon = [locationInfo[@"lon"] floatValue];
    
    self.locationMapView.accessibilityElementsHidden = NO;
    
    //disable user interaction with map view
    self.locationMapView.settings.scrollGestures = NO;
    self.locationMapView.settings.zoomGestures = NO;
    self.locationMapView.settings.tiltGestures = NO;
    self.locationMapView.settings.rotateGestures = NO;
    
    //place marker on location
    GMSMarker *marker = [[GMSMarker alloc] init];
    marker.position = CLLocationCoordinate2DMake(lat, lon);
    marker.title = [NSString stringWithFormat:@"%@", locationInfo[@"name"]];
    if (locationInfo[@"level"] != [NSNull null]) {
        marker.snippet = [NSString stringWithFormat:@"Level %@", locationInfo[@"level"]];
    }
    marker.map = self.locationMapView;
    [self.locationMapView setSelectedMarker:marker];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithTarget:marker.position
                                                               zoom:17
                                                            bearing:24
                                                       viewingAngle:0];
    self.locationMapView.camera = camera;
    
    if ([orderInfo[@"items"] count] == 0) {
        self.itemsInOrderLabel.hidden = YES;
    }
    
    [self updateFontSizes];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.orderDetails[@"order_details"][@"items"] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 48;
    
    NSString *preferredSize = [[UIApplication sharedApplication] preferredContentSizeCategory];
    float offset = 1;
    
    if ([preferredSize isEqualToString:UIContentSizeCategoryExtraSmall]) {
        offset = 0.9;
    } else if ([preferredSize isEqualToString:UIContentSizeCategorySmall]) {
        offset = 0.95;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryMedium]) {
        offset = 1.0;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryLarge]) {
        offset = 1.1;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraLarge]) {
        offset = 1.2;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraLarge]) {
        offset = 1.3;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge]) {
        offset = 1.4;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityMedium]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge]) {
        offset = 1.5;
    }
    
    cellHeight = cellHeight * offset;
    
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        
        //add vibrant effect for cell highlight
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVibrancyEffect *vibrantEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrantEffect];
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UIView *coloredView = [[UIView alloc] init];
        coloredView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        coloredView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [visualEffectView.contentView addSubview:coloredView];
        cell.selectedBackgroundView = visualEffectView;
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:248/255.0 alpha:1.0];
        
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:235/255.0 alpha:1.0];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 32) {
        bodyFont = [bodyFont fontWithSize:32];
    }
    cell.textLabel.font = bodyFont;
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", self.orderDetails[@"order_details"][@"items"][indexPath.row][@"MenuItemName"]];
    
    if ([self.orderDetails[@"order_details"][@"items"][indexPath.row][@"item_mods"] count] > 0) {
        cell.userInteractionEnabled = NO;
        cell.accessoryType = UITableViewCellAccessoryNone;
    } else {
        cell.userInteractionEnabled = YES;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show Item Details"]) {
        if ([segue.destinationViewController isKindOfClass:[ItemDetailsTableViewController class]]) {
            ItemDetailsTableViewController *itemDetailsTVC = (ItemDetailsTableViewController *)segue.destinationViewController;
            NSInteger selectedIndex = [self.tableView indexPathForSelectedRow].row;
            NSArray *itemsInOrder = self.orderDetails[@"order_details"][@"items"];
            itemDetailsTVC.title = [NSString stringWithFormat:@"%@", itemsInOrder[selectedIndex][@"MenuItemName"]];
            itemDetailsTVC.itemDetails = itemsInOrder[selectedIndex][@"mod_items"];
        }
    }
}

#pragma mark - Methods

- (void)didTapPhone:(UIButton *)sender {
    NSString *phoneNumber = [NSString stringWithFormat:@"%@", self.orderDetails[@"order_details"][@"OrderSummaryPhone"]];
    NSString *phoneNumberURL = [@"telprompt://" stringByAppendingString:phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumberURL]];
}

- (IBAction)didTapClose:(UIBarButtonItem *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self updateFontSizes];
}

- (void)updateFontSizes {
    UIFont *headlineFont = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    headlineFont = [headlineFont fontWithSize:headlineFont.pointSize * 1.65];
    if (headlineFont.pointSize > 33) {
        headlineFont = [headlineFont fontWithSize:33];
    }
    self.customerNameLabel.font = headlineFont;
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 22) {
        bodyFont = [bodyFont fontWithSize:22];
    }
    self.phoneNumberButton.titleLabel.font = bodyFont;
    
    NSMutableAttributedString *orderNumberString = [self.orderNumberLabel.attributedText mutableCopy];
    CGFloat orderLabelFontSize = headlineFont.pointSize / 1.25;
    NSDictionary *orderLabelDict = @{NSFontAttributeName:[bodyFont fontWithSize:orderLabelFontSize]};
    NSDictionary *orderNumberDict = @{NSFontAttributeName:[headlineFont fontWithSize:orderLabelFontSize]};
    [orderNumberString setAttributes:orderLabelDict range:NSMakeRange(0, 5)]; //set attributes for "order"
    [orderNumberString setAttributes:orderNumberDict range:NSMakeRange(6, orderNumberString.length-6)]; //set attributes for #000
    self.orderNumberLabel.attributedText = orderNumberString;
    
    UIFont *footnoteFont = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
    if (footnoteFont.pointSize > 22) {
        footnoteFont = [footnoteFont fontWithSize:22];
    }
    self.placedAtLabel.font = footnoteFont;
    self.pickedUpAtLabel.font = footnoteFont;
    self.deliveredAtLabel.font = footnoteFont;
    
    UIFont *subheadFont = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    if (subheadFont.pointSize > 17) {
        subheadFont = [subheadFont fontWithSize:17];
    }
    self.locationHeadingLabel.font = subheadFont;
    self.itemsInOrderLabel.font = subheadFont;
    
    [self.tableView reloadData];
    
    //update header height to show all contents
    [self.tableView.tableHeaderView setNeedsLayout];
    [self.tableView.tableHeaderView layoutIfNeeded];
    CGFloat newHeight = [self.tableView.tableHeaderView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
    
    CGRect headerFrame = self.tableView.tableHeaderView.frame;
    headerFrame.size.height = newHeight;
    self.tableView.tableHeaderView.frame = headerFrame;
    [self.tableView setTableHeaderView:self.tableView.tableHeaderView];
}


@end
