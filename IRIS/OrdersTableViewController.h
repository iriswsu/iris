//
//  OrdersTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/17/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Table view controller that shows the pending orders
 */
@interface OrdersTableViewController : UITableViewController

@end
