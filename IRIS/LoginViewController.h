//
//  LoginViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/15/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The first view controller visibile upon launch, shows the login screen
 */
@interface LoginViewController : UIViewController <UITextFieldDelegate>

@end
