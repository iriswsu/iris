//
//  OrdersTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 1/17/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <GoogleMaps/GoogleMaps.h>
#import "OrdersTableViewController.h"
#import "OrderTableViewCell.h"
#import "OrderDetailsTableViewController.h"
#import "NavViewController.h"
#import "AFNetworking.h"
#import "Destination.h"
#import "NavigatablePath.h"
#import "NavigationNode.h"
#import "WiFiLocation.h"
#import "APISettings.h"
#import "OrdersAPISettings.h"

@interface OrdersTableViewController ()

@property (nonatomic, strong) NSMutableArray *pendingOrders;
@property (nonatomic, strong) NSMutableArray *pickedUpOrders;
@property (nonatomic, strong) NSMutableArray *deliveredOrders;
@property (nonatomic, strong) UILabel *noOrdersLabel;

@end


@implementation OrdersTableViewController

static NSString* const CellIdentifier = @"OrderTableViewCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tableView registerNib:[UINib nibWithNibName:CellIdentifier bundle:nil]
         forCellReuseIdentifier:CellIdentifier];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.frame;
        self.tableView.backgroundView = blurEffectView;
        
        self.refreshControl.layer.zPosition = self.tableView.backgroundView.layer.zPosition + 1;
        
        self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    }
    
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    self.tableView.estimatedRowHeight = 120;
    
    [self.refreshControl beginRefreshing];
    [self fetchOrders];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.pendingOrders.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    OrderTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        
        //add vibrant effect for cell highlight
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVibrancyEffect *vibrantEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrantEffect];
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UIView *coloredView = [[UIView alloc] init];
        coloredView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        coloredView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [visualEffectView.contentView addSubview:coloredView];
        cell.selectedBackgroundView = visualEffectView;
    } else {
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:235/255.0 alpha:1.0];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    //add data to cells
    NSDictionary *orderDetails = self.pendingOrders[indexPath.row][@"order_details"];
    
    //convert date ordered to appropriate format
    NSDateFormatter *originalFormatter = [[NSDateFormatter alloc] init];
    originalFormatter.dateFormat = @"yyyy-MM-dd H:m:s";
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateStyle = NSDateFormatterNoStyle;
    timeFormatter.timeStyle = NSDateFormatterShortStyle;
    
    NSDate *date = [originalFormatter dateFromString:[NSString stringWithFormat:@"%@", orderDetails[@"OrderSummaryCreatedOn"][@"date"]]];
    cell.timeLabel.text = [timeFormatter stringFromDate:date];
    cell.orderNumberLabel.text = [NSString stringWithFormat:@"%@", orderDetails[@"OrderSummaryTicketNumber"]];
    cell.customerNameLabel.text = [NSString stringWithFormat:@"%@ %@", orderDetails[@"OrderSummaryFirstName"], orderDetails[@"OrderSummaryLastName"]];
    
    NSDictionary *locationDetails = self.pendingOrders[indexPath.row][@"order_location_details"][@"location_data"];
    NSMutableString *orderDestination = [NSMutableString stringWithFormat:@"%@", locationDetails[@"name"]];
    if (locationDetails[@"level"] != [NSNull null]) {
        [orderDestination appendString:[NSString stringWithFormat:@"\nLevel %@", locationDetails[@"level"]]];
    }
    cell.locationLabel.text = orderDestination;
    
    //font sizing
    UIFont *captionFont = [UIFont preferredFontForTextStyle:UIFontTextStyleCaption1];
    if (captionFont.pointSize > 16) {
        captionFont = [captionFont fontWithSize:16];
    }
    cell.timeLabel.font = captionFont;
    
    UIFont *footnoteFont = [UIFont preferredFontForTextStyle:UIFontTextStyleFootnote];
    footnoteFont = [footnoteFont fontWithSize:footnoteFont.pointSize * 1.25];
    if (footnoteFont.pointSize > 22) {
        footnoteFont = [footnoteFont fontWithSize:22];
    }
    cell.locationLabel.font = footnoteFont;
    cell.orderNumberLabel.font = footnoteFont;
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    bodyFont = [bodyFont fontWithSize:bodyFont.pointSize * 1.25];
    if (bodyFont.pointSize > 28) {
        bodyFont = [bodyFont fontWithSize:28];
    }
    cell.customerNameLabel.font = bodyFont;
    
    cell.orderNumberView.backgroundColor = [self.view.tintColor colorWithAlphaComponent:0.75];
    cell.orderNumberView.layer.cornerRadius = cell.orderNumberView.frame.size.width / 2.0;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"Show Order Details" sender:[tableView cellForRowAtIndexPath:indexPath]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self markOrderAsPickedUp:indexPath.row];
}

#pragma mark - Methods

- (IBAction)didTapClose:(UIBarButtonItem *)sender {
    [self.presentingViewController dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)didPullToRefresh:(UIRefreshControl *)sender {
    [self fetchOrders];
}

- (void)fetchOrders {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"api_username": ORDERS_API_USER, @"api_pass": ORDERS_API_PASS };
    [manager POST:[ORDERS_API_URL stringByAppendingString:ORDERS_API_GET_ORDER_DETAILS]
                   parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self.refreshControl endRefreshing];
        
       self.pendingOrders = [[NSMutableArray alloc] init];
       self.pickedUpOrders = [[NSMutableArray alloc] init];
       self.deliveredOrders = [[NSMutableArray alloc] init];
                       
        // store the orders data
        NSArray *ordersDictionary = responseObject;
        for (NSDictionary *order in ordersDictionary) {
            if (order[@"order_details"][@"OrderSummaryOrderPickedUp"] == [NSNull null]) {
               [self.pendingOrders addObject:order];
            } else if (order[@"order_details"][@"OrderSummaryOrderDelivered"] != [NSNull null]) {
               [self.deliveredOrders addObject:order];
            } else {
               [self.pickedUpOrders addObject:order];
            }
        }
        
        if (self.pendingOrders.count == 0) {
            if (self.tableView.tableHeaderView == nil) {
                self.tableView.tableHeaderView = self.noOrdersLabel;
            }
        } else {
            self.tableView.tableHeaderView = nil;
            [self.noOrdersLabel removeFromSuperview];
            self.noOrdersLabel = nil;
        }
        
        [self.tableView reloadData];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.refreshControl endRefreshing];
        
        // Display a failed alert message to the user
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Load Orders"
                                                                       message:[error localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleCancel
                                                handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
                                                    [self fetchOrders];
                                                }]];
        [self presentViewController:alert animated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *notification) {
                                                          [alert dismissViewControllerAnimated:NO completion:NULL];
                                                      }];
    }];
}

- (void)markOrderAsPickedUp:(NSInteger)orderRow {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *orderSummaryID = [NSString stringWithFormat:@"%@", self.pendingOrders[orderRow][@"order_details"][@"OrderSummaryID"]];
    NSDictionary *parameters = @{ @"api_username": ORDERS_API_USER,
                                  @"api_pass": ORDERS_API_PASS,
                                  @"order_summary_id": orderSummaryID };
    [manager POST:[ORDERS_API_URL stringByAppendingString:ORDERS_API_MARK_ORDER_PICKED_UP]
       parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
           //order successfully marked as picked up, dismiss to begin routing
           [self performSegueWithIdentifier:@"Unwind From Orders" sender:[self.tableView cellForRowAtIndexPath:[self.tableView indexPathForSelectedRow]]];
       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           //failed to mark as picked up
           UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Pick Up Order"
                                                                          message:[error localizedDescription]
                                                                   preferredStyle:UIAlertControllerStyleAlert];
           [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil]];
           [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [self markOrderAsPickedUp:orderRow];
                                                   }]];
           [self presentViewController:alert animated:YES completion:NULL];
           [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                             object:nil
                                                              queue:[NSOperationQueue mainQueue]
                                                         usingBlock:^(NSNotification *notification) {
                                                             [alert dismissViewControllerAnimated:NO completion:NULL];
                                                         }];
       }];
}

- (void)preferredFontsChanged:(NSNotification *)notification {
    //reload data to update font sizes
    [self.tableView reloadData];
}

#pragma mark - Getters and Setters

- (UILabel *)noOrdersLabel {
    if (!_noOrdersLabel) {
        _noOrdersLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width,
                                                                  self.tableView.bounds.size.height
                                                                  - self.navigationController.navigationBar.frame.size.height
                                                                  - [UIApplication sharedApplication].statusBarFrame.size.height)];
        _noOrdersLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        _noOrdersLabel.text = @"No Orders";
        _noOrdersLabel.textAlignment = NSTextAlignmentCenter;
        _noOrdersLabel.textColor = [UIColor darkGrayColor];
        _noOrdersLabel.font = [UIFont systemFontOfSize:44];
    }
    
    return _noOrdersLabel;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Unwind From Orders"]) {
        if ([segue.destinationViewController isKindOfClass:[NavViewController class]]) {
            NavViewController *navVC = (NavViewController *)segue.destinationViewController;
            navVC.orderInfo = self.pendingOrders[[self.tableView indexPathForCell:sender].row];
            navVC.destinationInfo = self.pendingOrders[[self.tableView indexPathForCell:sender].row][@"order_location_details"];
        }
    } else if ([segue.identifier isEqualToString:@"Show Order Details"]) {
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
            if ([navController.viewControllers[0] isKindOfClass:[OrderDetailsTableViewController class]]) {
                OrderDetailsTableViewController *orderDetailsTVC = (OrderDetailsTableViewController *)navController.viewControllers[0];
                //remove X so back button will appear
                orderDetailsTVC.navigationItem.leftBarButtonItem = nil;
                orderDetailsTVC.orderDetails = self.pendingOrders[[self.tableView indexPathForCell:sender].row];
            }
        }
    }
}


@end
