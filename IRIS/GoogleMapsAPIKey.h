//
//  GoogleMapsAPIKey.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/18/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//
//  Defines the Google Maps API key for the project in such a way that each developer can use their own key.

#import <Foundation/Foundation.h>

@interface GoogleMapsAPIKey : NSObject

extern NSString * const GOOGLE_MAPS_API_KEY;
extern NSString * const GOOGLE_MAPS_SERVER_DIRECTIONS_API_KEY;

@end
