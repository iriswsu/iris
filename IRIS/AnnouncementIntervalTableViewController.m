//
//  AnnouncementIntervalTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 3/23/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "AnnouncementIntervalTableViewController.h"
#import "NavViewController.h"

@interface AnnouncementIntervalTableViewController ()

@property (nonatomic, strong) NSArray *intervalOptions;

@end

@implementation AnnouncementIntervalTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.frame;
        self.tableView.backgroundView = blurEffectView;
        
        self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    }
    
    self.intervalOptions = @[@10, @15, @20, @30, @40, @60, @90];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.intervalOptions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 48;
    
    NSString *preferredSize = [[UIApplication sharedApplication] preferredContentSizeCategory];
    float offset = 1;
    
    if ([preferredSize isEqualToString:UIContentSizeCategoryExtraSmall]) {
        offset = 0.9;
    } else if ([preferredSize isEqualToString:UIContentSizeCategorySmall]) {
        offset = 0.95;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryMedium]) {
        offset = 1.0;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryLarge]) {
        offset = 1.1;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraLarge]) {
        offset = 1.2;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraLarge]) {
        offset = 1.3;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge]) {
        offset = 1.4;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityMedium]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge]) {
        offset = 1.5;
    }
    
    cellHeight = cellHeight * offset;
    
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        
        //add vibrant effect for cell highlight
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVibrancyEffect *vibrantEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrantEffect];
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UIView *coloredView = [[UIView alloc] init];
        coloredView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        coloredView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [visualEffectView.contentView addSubview:coloredView];
        cell.selectedBackgroundView = visualEffectView;
    } else {
        cell.backgroundColor = [UIColor whiteColor];
        
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:235/255.0 alpha:1.0];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"%ld Seconds", (long)[self.intervalOptions[indexPath.row] integerValue]];
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 40) {
        bodyFont = [bodyFont fontWithSize:40];
    }
    cell.textLabel.font = bodyFont;
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults integerForKey:ContinuousPromptsIntervalKey] == [self.intervalOptions[indexPath.row] integerValue]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    for (UITableViewCell *cell in tableView.visibleCells) {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:self.intervalOptions[indexPath.row] forKey:ContinuousPromptsIntervalKey];
    [userDefaults synchronize];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Methods

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

@end
