//
//  OrderTableViewCell.m
//  IRIS
//
//  Created by Jordan Hipwell on 1/17/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "OrderTableViewCell.h"



@implementation OrderTableViewCell

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    //prevent animating subviews' backgroundColor to nil for selected cell state
    UIColor *backgroundColor = self.orderNumberView.backgroundColor;
    [super setHighlighted:highlighted animated:animated];
    self.orderNumberView.backgroundColor = backgroundColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    UIColor *backgroundColor = self.orderNumberView.backgroundColor;
    [super setSelected:selected animated:animated];
    self.orderNumberView.backgroundColor = backgroundColor;
}

- (NSString *)accessibilityLabel {
    return [NSString stringWithFormat:@"Order %@. %@. %@. %@.", self.orderNumberLabel.text, self.timeLabel.text, self.customerNameLabel.text, self.locationLabel.text];
}

@end
