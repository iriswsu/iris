//
//  OrderTableViewCell.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/17/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Custom cell to display order information in the Orders screen
 */
@interface OrderTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *orderNumberView;
@property (weak, nonatomic) IBOutlet UILabel *orderNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;

@end
