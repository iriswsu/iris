//
//  LoginViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 1/15/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "LoginViewController.h"
#import "CredentialsTableViewController.h"
#import "NavViewController.h"
#import "OrdersAPISettings.h"
#import "AFNetworking.h"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIButton *logInButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (assign, nonatomic) CGPoint originalCenterLocation;

@property (strong, nonatomic) CredentialsTableViewController *credentialsTVC;

@property (strong, nonatomic) NSDictionary *userInfo; //information about the logged in user's account

@end


@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.logoImageView.alpha = 0.25;
    self.containerView.alpha = 0;
    self.logInButton.alpha = 0;
    
    [self updateFontSizes];
    
    self.activityIndicator.alpha = 0;
    self.activityIndicator.hidden = YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillChangeFrame:)
                                                 name:UIKeyboardWillChangeFrameNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userEnteredText:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.originalCenterLocation = self.view.center;
    
    self.credentialsTVC.usernameTextField.delegate = self;
    self.credentialsTVC.passwordTextField.delegate = self;
    
    [UIView animateWithDuration:1.0 animations:^{
        self.logoImageView.alpha = 1;
        self.containerView.alpha = 1;
        self.logInButton.alpha = 1;
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillChangeFrameNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UITextFieldTextDidChangeNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    //if user pressed Next with username focused
    if ([textField isEqual:self.credentialsTVC.usernameTextField]) {
        [self.credentialsTVC.passwordTextField becomeFirstResponder];
    }
    //if user pressed Go with password focused
    else if ([textField isEqual:self.credentialsTVC.passwordTextField]) {
        [textField resignFirstResponder];
        [self didTapLogIn];
    }
    return NO;
}

#pragma mark - Methods

- (IBAction)didTapBackground {
    [self.containerView endEditing:YES]; //dismiss keyboard
}

- (IBAction)didTapLogIn {
    [self.containerView endEditing:YES];
    
    self.logInButton.enabled = NO;
    self.activityIndicator.hidden = NO;
    [self.activityIndicator startAnimating];
    
    [UIView animateWithDuration:0.4 animations:^{
        self.activityIndicator.alpha = 1;
        self.logInButton.alpha = 0;
    } completion:^(BOOL finished) {
        if (finished) {
            self.logInButton.hidden = YES;
            self.activityIndicator.hidden = NO;
        }
    }];
    
    //verify credentials are correct
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{ @"api_username": ORDERS_API_USER, @"api_pass": ORDERS_API_PASS, @"emp_username": self.credentialsTVC.usernameTextField.text, @"emp_pass": self.credentialsTVC.passwordTextField.text };
    [manager POST:[ORDERS_API_URL stringByAppendingString:ORDERS_API_VERIFY_USER]
       parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
           //communication was successful
           NSDictionary *responseDict = responseObject;
         
           if ([[responseDict valueForKey:@"authenticated"] isEqualToString:@"true"]) {
               self.userInfo = responseDict;
               [self performSegueWithIdentifier:@"Show Map" sender:self];
           } else {
               //invalid credentials
               self.logInButton.hidden = NO;
               self.logInButton.enabled = YES;
               
               [UIView animateWithDuration:0.4 animations:^{
                   self.activityIndicator.alpha = 0;
                   self.logInButton.alpha = 1;
               } completion:^(BOOL finished) {
                   if (finished) {
                       self.logInButton.hidden = NO;
                       self.activityIndicator.hidden = YES;
                   }
               }];
               
               UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Log In"
                                                                             message:@"Either the username or password is incorrect.\nPlease try again."
                                                                      preferredStyle:UIAlertControllerStyleAlert];
               [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                        style:UIAlertActionStyleCancel
                                                      handler:nil]];
               [self presentViewController:alert animated:YES completion:NULL];
               //automatically dismiss when closing app
               [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                                object:nil
                                                                 queue:[NSOperationQueue mainQueue]
                                                            usingBlock:^(NSNotification *notification) {
                                                                [alert dismissViewControllerAnimated:NO completion:NULL];
                                                            }];
           }

       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           //communication failed
           self.logInButton.hidden = NO;
           self.logInButton.enabled = YES;
   
           [UIView animateWithDuration:0.4 animations:^{
               self.activityIndicator.alpha = 0;
               self.logInButton.alpha = 1;
           } completion:^(BOOL finished) {
               if (finished) {
                   self.logInButton.hidden = NO;
                   self.activityIndicator.hidden = YES;
               }
           }];
           
           UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Log In"
                                                                          message:[error localizedDescription]
                                                                   preferredStyle:UIAlertControllerStyleAlert];
           [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil]];
           [self presentViewController:alert animated:YES completion:NULL];
           [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                             object:nil
                                                              queue:[NSOperationQueue mainQueue]
                                                         usingBlock:^(NSNotification *notification) {
                                                             [alert dismissViewControllerAnimated:NO completion:NULL];
                                                         }];
       }];
    
}

- (void)userEnteredText:(NSNotification *)notification {
    self.logInButton.enabled = ![[self.credentialsTVC.usernameTextField text] isEqualToString:@""]
                                && ![[self.credentialsTVC.passwordTextField text] isEqualToString:@""];
}

- (void)keyboardWillChangeFrame:(NSNotification *)notification {
    //adjust view to ensure keyboard doesn't cover up interface
    NSTimeInterval keyboardAnimationDuration;
    UIViewAnimationCurve keyboardAnimationCurve;
    
    [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&keyboardAnimationDuration];
    [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardAnimationCurve];
    
    CGFloat screenHeight = [UIScreen mainScreen].bounds.size.height;
    CGSize keyboardSize = [[[notification userInfo] valueForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGFloat keyboardHeight = MIN(keyboardSize.height, keyboardSize.width);
    
    CGFloat lowestLocation = self.logInButton.frame.origin.y + self.logInButton.frame.size.height + 20;
    
    //animate view upwards if keyboard would cover up credentials
    if (screenHeight - keyboardHeight < lowestLocation) {
        CGFloat centerPosition = self.originalCenterLocation.y - (lowestLocation - (screenHeight - keyboardHeight));
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:(keyboardAnimationDuration)];
        [UIView setAnimationCurve:(keyboardAnimationCurve)];
        self.view.center = CGPointMake(self.originalCenterLocation.x, centerPosition);
        [UIView commitAnimations];
    }
}

- (void)keyboardWillHide:(NSNotification *)notification {
    //reset view position when keyboard hides
    NSTimeInterval keyboardAnimationDuration;
    UIViewAnimationCurve keyboardAnimationCurve;
    
    [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&keyboardAnimationDuration];
    [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&keyboardAnimationCurve];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:keyboardAnimationDuration];
    [UIView setAnimationCurve:keyboardAnimationCurve];
    self.view.center = self.originalCenterLocation;
    [UIView commitAnimations];
}

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self updateFontSizes];
}

- (void)updateFontSizes {
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    bodyFont = [bodyFont fontWithSize:bodyFont.pointSize * 1.4];
    if (bodyFont.pointSize > 40) {
        bodyFont = [bodyFont fontWithSize:40];
    }
    self.logInButton.titleLabel.font = bodyFont;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show Credentials"]) {
        if ([segue.destinationViewController isKindOfClass:[CredentialsTableViewController class]]) {
            self.credentialsTVC = segue.destinationViewController;
        }
    } else if ([segue.identifier isEqualToString:@"Show Map"]) {
        if ([segue.destinationViewController isKindOfClass:[UINavigationController class]]) {
            UINavigationController *navController = segue.destinationViewController;
            if ([navController.topViewController isKindOfClass:[NavViewController class]]) {
                NavViewController *navVC = (NavViewController *)navController.topViewController;
                navVC.userInfo = self.userInfo;
            }
        }
    }
}

- (IBAction)unwindFromMap:(UIStoryboardSegue *)segue {
    //user did log out
    self.credentialsTVC.usernameTextField.text = @"";
    self.credentialsTVC.passwordTextField.text = @"";
    [self userEnteredText:nil];
    self.activityIndicator.alpha = 0;
    self.activityIndicator.hidden = YES;
    self.logInButton.alpha = 1;
    self.logInButton.hidden = NO;
}

@end
