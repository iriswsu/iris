//
//  KHGoogleMapsAPIKey.m
//  WSU
//
//  Created by Zach Thurston on 6/30/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import "GoogleMapsAPIKey.h"

@implementation GoogleMapsAPIKey

// Put your API keys in these strings, but don't commit this file to the project
// Get these from console.developers.google.com
// Create a new project, click APIs & Auth, click Credentails, then Create new key under Public API access
// Do iOS key first then put in the bundle identifier: edu.weber.cs.IRIS, then put that key below
// Create new key > Server key, leave the box blank and hit Create, put that API key in for directions
// Click on APIs then enable Directions API and Google Maps SDK for iOS

NSString * const GOOGLE_MAPS_API_KEY = @"";
NSString * const GOOGLE_MAPS_SERVER_DIRECTIONS_API_KEY = @"";

@end