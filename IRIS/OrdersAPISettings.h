//
//  OrdersAPISettings.h
//  IRIS
//
//  Created by Mac Attack on 3/6/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface OrdersAPISettings : NSObject

extern NSString *const ORDERS_API_URL;

extern NSString *const ORDERS_API_GET_ORDER_DETAILS;
extern NSString *const ORDERS_API_VERIFY_USER;
extern NSString *const ORDERS_API_MARK_ORDER_PICKED_UP;
extern NSString *const ORDERS_API_MARK_ORDER_DELIVERED;

extern NSString *const ORDERS_API_USER;
extern NSString *const ORDERS_API_PASS;

@end
