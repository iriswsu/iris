//
//  CredentialsTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 1/15/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "CredentialsTableViewController.h"

@interface CredentialsTableViewController ()

@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;

@end


@implementation CredentialsTableViewController

- (void)viewDidLoad {
    [self updateFontSizes];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    cell.layer.borderWidth = 1.0 / [UIScreen mainScreen].scale; //hairline border
    cell.layer.borderColor = [[UIColor blackColor] colorWithAlphaComponent:0.25].CGColor;
}

#pragma mark - Methods

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self updateFontSizes];
}

- (void)updateFontSizes {
    UIFont *headlineFont = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    if (headlineFont.pointSize > 23) {
        headlineFont = [headlineFont fontWithSize:23];
    }
    self.usernameLabel.font = headlineFont;
    self.passwordLabel.font = headlineFont;
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 28) {
        bodyFont = [bodyFont fontWithSize:28];
    }
    self.usernameTextField.font = bodyFont;
    self.passwordTextField.font = bodyFont;
}

@end
