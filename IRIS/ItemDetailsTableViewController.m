//
//  ItemDetailsTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "ItemDetailsTableViewController.h"

@interface ItemDetailsTableViewController ()

@property (weak, nonatomic) IBOutlet UITextView *additionalInstructionsTextView;

@end

@implementation ItemDetailsTableViewController

static NSString* const CellIdentifier = @"ItemDetailsCell";


- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.additionalInstructionsTextView.backgroundColor = [UIColor clearColor];
        
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.frame;
        self.tableView.backgroundView = blurEffectView;
        
        self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    }
    
    self.additionalInstructionsTextView.textContainerInset = UIEdgeInsetsMake(40, 15, 0, 15);
    [self.additionalInstructionsTextView sizeToFit];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.itemDetails count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 48;
    
    NSString *preferredSize = [[UIApplication sharedApplication] preferredContentSizeCategory];
    float offset = 1;
    
    if ([preferredSize isEqualToString:UIContentSizeCategoryExtraSmall]) {
        offset = 0.9;
    } else if ([preferredSize isEqualToString:UIContentSizeCategorySmall]) {
        offset = 0.95;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryMedium]) {
        offset = 1.0;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryLarge]) {
        offset = 1.1;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraLarge]) {
        offset = 1.2;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraLarge]) {
        offset = 1.3;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge]) {
        offset = 1.4;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityMedium]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge]) {
        offset = 1.5;
    }
    
    cellHeight = cellHeight * offset;
    
    return cellHeight;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
    } else {
        cell.backgroundColor = [UIColor colorWithWhite:248/255.0 alpha:1.0];
    }
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.textLabel.text = [NSString stringWithFormat:@"%@", self.itemDetails[indexPath.row][@"ItemDescription"]];
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 32) {
        bodyFont = [bodyFont fontWithSize:32];
    }
    cell.textLabel.font = bodyFont;
    
    return cell;
}

#pragma mark - Methods

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

@end
