//
//  AnnouncementIntervalTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 3/23/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Table view controller that shows the possible announcement interval options
 */
@interface AnnouncementIntervalTableViewController : UITableViewController

@end
