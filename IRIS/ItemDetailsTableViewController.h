//
//  ItemDetailsTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 2/2/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Table view controller that shows the item details for an order
 */
@interface ItemDetailsTableViewController : UITableViewController

@property (nonatomic, strong) NSArray *itemDetails;

@end
