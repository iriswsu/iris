//
//  APISettings.h
//  IRIS
//
//  Created by Zach Thurston on 6/30/14.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface APISettings : NSObject
extern double const WSU_OGDEN_MAIN_LATITUDE;
extern double const WSU_OGDEN_MAIN_LONGITUDE;
extern NSString * const NAV_API_URL;
extern NSString * const NAV_API_USER;
extern NSString * const NAV_API_PASS;
@end