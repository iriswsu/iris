//
//  NavigationNode.m
//  WSU
//
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import "NavigationNode.h"

@implementation NavigationNode

- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary{
    self.latitude = [[apiDictionary objectForKey:@"lat"] doubleValue];
    self.longitude = [[apiDictionary objectForKey:@"lon"] doubleValue];
    self.name = [apiDictionary objectForKey:@"name"];
    
    return self;
}

@end