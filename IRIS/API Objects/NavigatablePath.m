//
//  NavigatiablePath.m
//  WSU
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import "NavigatablePath.h"

@implementation NavigatablePath

// initialize automatically using a dictionary returned by the API
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary{
    self.start =  [NSURL URLWithString:[apiDictionary objectForKey:@"start"]];
    self.end = [NSURL URLWithString:[apiDictionary objectForKey:@"end"]];
    
    self.weight = [[apiDictionary objectForKey:@"weight"] doubleValue];
    self.length = [[apiDictionary objectForKey:@"length"] doubleValue];
    
    // relationship is an array of NSURLs
    NSArray *tmpRelationships = [apiDictionary objectForKey:@"relationships"];
    
    self.relationships = [[NSMutableArray alloc] init];
    for (int i = 0; i < [tmpRelationships count]; i++) {
        [self.relationships addObject:[NSURL URLWithString:[tmpRelationships objectAtIndex:i]]];
    }
    
    // path is an array of NavigationNodes
    NSArray *tmpPath = [apiDictionary objectForKey:@"path"];
    
    self.path = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [tmpPath count]; i++) {
        [self.path addObject:[[NavigationNode alloc] initWithAPIDictionary:[tmpPath objectAtIndex:i]]];
    }
    
    // nodes is an array of NSURLs
    self.nodes = [apiDictionary objectForKey:@"nodes"];
    
    //for (int i = 0; i < [tmpRelationships count]; i++) {
    //    [self.nodes addObject:[NSURL URLWithString:[tmpNodes objectAtIndex:i]]];
    //}
    
    return self;
}

@end
