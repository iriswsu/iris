//
//  Destination.h
//  WSU
//
//  This class mirrors the results returned by the API
//
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Weber State University. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Destination : NSObject

@property (nonatomic, retain) NSString *destinationID;
@property (nonatomic, retain) NSString *name;
@property (nonatomic, retain) NSString *type;
@property (nonatomic, retain) NSString *abbreviation;
@property (nonatomic, retain) NSString *level; //this property used to be called floor
@property BOOL active;
@property double latitude;
@property double longitude;
@property (nonatomic, retain) NSURL *URI;
@property (nonatomic, retain) NSArray *tags;


// initialize automatically using a dictionary returned by the API
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary;

@end
