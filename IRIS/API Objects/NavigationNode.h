//
//  NavigationNode.h
//  WSU
//
//  Represents the nodes returned inside paths by the API
//
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@interface NavigationNode : NSObject

@property double latitude;
@property double longitude;
@property (nonatomic, retain) NSString *name;

// takes the dictionary returned by the api and fills in the object's properties
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary;

@end
