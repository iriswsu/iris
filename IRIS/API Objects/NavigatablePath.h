//
//  NavigatiablePath.h
//  WSU
//
//  This class mirrors the navigatable paths returned by the API, for use in displaying how to get from a point to another on campus
//
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NavigationNode.h"

@interface NavigatablePath : NSObject

@property (nonatomic, retain) NSMutableArray *relationships;
@property (nonatomic, retain) NSURL *start;
@property (nonatomic, retain) NSURL *end;
@property double weight;
@property double length;
@property (nonatomic, retain) NSMutableArray *path;
@property (nonatomic, retain) NSMutableArray *nodes;

// initialize automatically using a dictionary returned by the API
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary;

@end