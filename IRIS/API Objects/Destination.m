//
//  Destination.m
//  WSU
//
//  Created by Zach Thurston on 7/8/14.
//  Copyright (c) 2014 Weber State University. All rights reserved.
//

#import "Destination.h"

@implementation Destination

// initialize the object using a dictionary that would be returned by the API
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary{
    // pull the data out of the dictionary, per the api v1 doc
    self.destinationID = [apiDictionary objectForKey:@"id"];
    self.URI = [apiDictionary objectForKey:@"self"];
    
    NSDictionary *tmpData = [apiDictionary objectForKey:@"data"];
    
    self.abbreviation = [tmpData objectForKey:@"abbrv"];
    self.name = [tmpData objectForKey:@"name"];
    self.level = [tmpData objectForKey:@"level"];
    self.tags = [tmpData objectForKey:@"tags"];
    self.latitude = [[tmpData objectForKey:@"lat"] doubleValue];
    self.longitude =  [[tmpData objectForKey:@"lon"] doubleValue];
    self.type = [tmpData objectForKey:@"type"];
    
    // set to yes if 1, no if 0, do nothing if not specified (should leave it as nil)
    if ([[tmpData objectForKey:@"active"] integerValue] == 1) {
        self.active = YES;
    } else if ([[tmpData objectForKey:@"active"] integerValue] == 0) {
        self.active = NO;
    }
    return self;
}

@end
