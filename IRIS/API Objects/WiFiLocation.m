//
//  WiFiLocation.m
//  WSU
//
//  Created by Zach Thurston on 7/16/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import "WiFiLocation.h"

@implementation WiFiLocation

- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary{
    self.campus = [apiDictionary objectForKey:@"campus"];
    self.building = [apiDictionary objectForKey:@"building"];
    self.floor = [apiDictionary objectForKey:@"floor"];
    // object for key is intentionally spelled wrong, per api spec
    self.latitude = [[apiDictionary objectForKey:@"lattitude"] doubleValue];
    self.longitude = [[apiDictionary objectForKey:@"longitude"] doubleValue];
    
    return self;
}

@end
