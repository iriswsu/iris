//
//  WiFiLocation.h
//  WSU
//
//  Created by Zach Thurston on 7/16/14.
//  Copyright (c) 2014 Klint Holmes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WiFiLocation : NSObject

@property (nonatomic, retain) NSString *campus;
@property (nonatomic, retain) NSString *building;
@property (nonatomic, retain) NSString *floor;

@property double latitude;
@property double longitude;

// initialize automatically using a dictionary returned by the API
// WARNING: Need to test this on the API before this is good to go -zthurston
- (id)initWithAPIDictionary:(NSDictionary *)apiDictionary;

@end
