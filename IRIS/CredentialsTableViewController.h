//
//  CredentialsTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/15/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 The table view controller displayed within the login view controller, providing text fields for username and password
 */
@interface CredentialsTableViewController : UITableViewController

@property (weak, nonatomic) IBOutlet UITextField *usernameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end
