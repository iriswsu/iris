//
//  NavViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 1/18/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "NavViewController.h"
#import <GoogleMaps/GoogleMaps.h>
#import <AVFoundation/AVFoundation.h>
#import "APISettings.h"
#import "GoogleMapsAPIKey.h"
#import "AFNetworking.h"
#import "NavigatablePath.h"
#import "OrderDetailsTableViewController.h"
#import "OrdersAPISettings.h"

@interface NavViewController ()

@property (weak, nonatomic) IBOutlet GMSMapView *mapView;

@property (weak, nonatomic) IBOutlet UIView *topView; //container view displayed at top, holds the navDirectionsView
@property (weak, nonatomic) IBOutlet UIView *navDirectionsView; //view that contains routing instruction labels
@property (strong, nonatomic) UIView *loadingLabelView; //view displayed in topView that indicates loading status
@property (weak, nonatomic) IBOutlet UIImageView *navImageView;
@property (weak, nonatomic) IBOutlet UILabel *arrivalLabel;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *directionsLabel;
@property (weak, nonatomic) IBOutlet UIToolbar *supplementaryToolbar; //toolbar that appears when an action is available (Deliver button))
@property (weak, nonatomic) IBOutlet UIBarButtonItem *supplementaryButton; //button displayed in supplementaryToolbar

@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *orderDetailsButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *ordersListButton;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *helpButton;

@property (strong, nonatomic) CLLocationManager *locationManager;

@property (assign, nonatomic) BOOL navigationStarted;
@property (assign, nonatomic) BOOL didRequestRoute;
@property (assign, nonatomic) BOOL announcedFirstDirection;
@property (assign, nonatomic, getter=isApproachingNode) BOOL approachingNode;
@property (assign, nonatomic, getter=isRightNextToNode) BOOL rightNextToNode;
@property (assign, nonatomic) BOOL shouldRemoveUpcomingNode;

@property (strong, nonatomic) GMSCameraPosition *mainCampusCamera; //camera showing all of the WSU campus
@property (strong, nonatomic) NavigatablePath *pathToNavigate;
@property (strong, nonatomic) NavigationNode *nodeToPrompt; //The next major node that the prompts will always be directing the user to. Once the user passes it, it is set to the next major node.

@property (strong, nonatomic) AVSpeechSynthesizer *speechSynthesizer;
@property (strong, nonatomic) NSTimer *continuousVoicePromptsTimer;

@end


@implementation NavViewController

NSString *const VoiceCommandsEnabledKey = @"VoiceCommandsEnabled";
NSString *const VoiceCommandsPitchKey = @"VoiceCommandsPitch";
NSString *const VoiceCommandsRateKey = @"VoiceCommandsRate";
NSString *const ContinuousPromptsEnabledKey = @"ContinuousPromptsEnabled";
NSString *const ContinuousPromptsIntervalKey = @"ContinuousPromptsInterval";

enum {
    NodeTypeNext,
    NodeTypeImmediate
}; typedef int NodeType;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *defaultPrefs = @{ VoiceCommandsEnabledKey: @YES,
                                    VoiceCommandsPitchKey: @1.1,
                                    VoiceCommandsRateKey: [NSNumber numberWithFloat:AVSpeechUtteranceDefaultSpeechRate / 3],
                                    ContinuousPromptsEnabledKey: @NO,
                                    ContinuousPromptsIntervalKey: @15 };
    [userDefaults registerDefaults:defaultPrefs];
    
    [self updateFontSizes];
    
    self.settingsButton.accessibilityLabel = @"Settings";
    self.orderDetailsButton.accessibilityLabel = @"Order Details";
    self.ordersListButton.accessibilityLabel = @"Orders List";
    self.helpButton.accessibilityLabel = @"Help";
    
    self.orderDetailsButton.enabled = NO;
    self.navDirectionsView.hidden = YES;
    
    self.mapView.camera = self.mainCampusCamera;
    
    //move maps buttons out from underneath toolbar and nav directions
    self.mapView.padding = UIEdgeInsetsMake(self.topView.frame.size.height, 0, self.navigationController.toolbar.frame.size.height, 0);
    
    //disable user interaction with map view
    [self.mapView.settings setAllGesturesEnabled:NO];
    
    //add accessibility support for items on map view
    self.mapView.accessibilityElementsHidden = NO;
    
    self.navDirectionsView.isAccessibilityElement = YES;
    self.navImageView.isAccessibilityElement = NO;
    self.arrivalLabel.isAccessibilityElement = NO;
    self.distanceLabel.isAccessibilityElement = NO;
    self.directionsLabel.isAccessibilityElement = NO;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.25 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"Show Orders" sender:self];
    });
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    self.mainCampusCamera = nil;
    self.speechSynthesizer = nil;
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
    NSString *title = nil;
    NSString *message = nil;
    
    switch (status) {
        case kCLAuthorizationStatusAuthorizedWhenInUse: {
            [self startUpdatingLocation];
            break;
        }
        case kCLAuthorizationStatusDenied: {
            title = @"Access Denied";
            message = @"Navigation cannot begin because IRIS does not have access to your location. Enable access in Settings to continue.";
            break;
        }
        case kCLAuthorizationStatusRestricted: {
            title = @"Restricted Access";
            message = @"Navigation cannot begin due to Location Services restrictions set on your device.";
            break;
        }
        default: {
            break;
        }
    }
    
    if (title != nil || message != nil) {
        [self stopNavigating];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                       message:message
                                                                preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                     style:UIAlertActionStyleDefault
                                                   handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:NULL];
        
        //auto dismiss alert when app is closed
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *notification) {
                                                          [alert dismissViewControllerAnimated:NO completion:NULL];
                                                      }];
    }
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    NSString *title = nil;
    NSString *message = nil;
    
    switch ([error code]) {
        case kCLErrorDenied:
            title = @"Access Denied";
            message = @"Unable to get your current location because IRIS does not have permission to use your location.";
            break;
        case kCLErrorNetwork:
            title = @"Network Error";
            message = @"Unable to get your current location because a network error occurred.";
            break;
        case kCLErrorLocationUnknown:
            return; //will keep trying to get location
        default:
            return; //ignore other errors
    }
    
    [self stopNavigating];

    UIAlertController *alert = [UIAlertController alertControllerWithTitle:title
                                                                  message:message
                                                           preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *ok = [UIAlertAction actionWithTitle:@"OK"
                                                 style:UIAlertActionStyleDefault
                                               handler:nil];
    [alert addAction:ok];
    [self presentViewController:alert animated:YES completion:NULL];

    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
        [alert dismissViewControllerAnimated:NO completion:NULL];
    }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading {
    [self.mapView animateToBearing:newHeading.trueHeading];
    
    //if a location has been recorded
    if (manager.location) {
        //if bearing is not invalid
        if (newHeading.trueHeading > 0) {
            GMSCameraPosition *newPosition = [GMSCameraPosition cameraWithTarget:manager.location.coordinate
                                                                            zoom:19
                                                                         bearing:newHeading.trueHeading
                                                                    viewingAngle:45];
            [self.mapView animateToCameraPosition:newPosition];
        }
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    if ([locations lastObject]) {
        CLLocation *currentLocation = [locations lastObject];
        
//        NSLog(@"location accuracy %f", currentLocation.horizontalAccuracy);
        if (!self.didRequestRoute) {
            //don't load route until we have an accurate location - within 20 meters
            if (currentLocation.horizontalAccuracy < 20 && currentLocation.horizontalAccuracy > 0) {
                [self getPathWithSource:currentLocation.coordinate andDestinationID:self.destinationInfo[@"OrderSummaryLocationID"]];
            }
        } else {
            if (self.pathToNavigate && self.pathToNavigate.path.count > 0) {
                // If there is more than one node left in the path, then find what way the user needs to head
                if (self.pathToNavigate.path.count > 1) {                    
                    // Calculate what direction the user needs to turn at the next upcoming turn
                    int direction = [self calculateTurnDirection:self.pathToNavigate.path];

                    if (direction == 0) {
                        self.directionsLabel.text = @"Turn left";
                        self.navImageView.image = [UIImage imageNamed:@"left"];
                    } else if (direction == 1) {
                        self.directionsLabel.text = @"Take a slight left";
                        self.navImageView.image = [UIImage imageNamed:@"slight-left"];
                    } else if (direction == 2) {
                        self.directionsLabel.text = @"Take a slight right";
                        self.navImageView.image = [UIImage imageNamed:@"slight-right"];
                    } else if (direction == 3) {
                        self.directionsLabel.text = @"Turn right";
                        self.navImageView.image = [UIImage imageNamed:@"right"];
                    } else if (direction == 4) {
                        self.directionsLabel.text = @"Continue straight";
                        self.navImageView.image = [UIImage imageNamed:@"up"];
                    }
                } else {
                    //next node is the destination
                    self.nodeToPrompt = self.pathToNavigate.path[0];
                    self.directionsLabel.text = @"Arrive at destination";
                    self.navImageView.image = [UIImage imageNamed:@"destination"];
                }
                
                //calculate feet until next node
                CLLocationCoordinate2D nextNodeCoordinate = CLLocationCoordinate2DMake(self.nodeToPrompt.latitude, self.nodeToPrompt.longitude);
                CLLocationDistance meters = GMSGeometryDistance(currentLocation.coordinate, nextNodeCoordinate);
                CLLocationDistance feet = meters * 3.2808;
                self.distanceLabel.text = [NSString stringWithFormat:@"%d feet", (int)feet];
                
                //calculate estimated arrival time
                NSMutableArray *pathList = [[NSMutableArray alloc] init];
                pathList = self.pathToNavigate.path;
                GMSMutablePath *path = [GMSMutablePath path];
                for (int i = 0; i < [pathList count]; i++) {
                    [path addLatitude:[(NavigationNode *)pathList[i] latitude] longitude:[(NavigationNode *)pathList[i]longitude]];
                }
                
                CLLocationDistance metersToDestination = GMSGeometryLength(path);
                CLLocationDistance milesRemaining = metersToDestination * 0.00062137;
                double minutesRemaining = (milesRemaining * 60) / 2.25;
                double secondsRemaining = minutesRemaining * 60;
                
                NSDate *arrivalTime = [[NSDate date] dateByAddingTimeInterval:secondsRemaining];
                NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
                timeFormatter.dateStyle = NSDateFormatterNoStyle;
                timeFormatter.timeStyle = NSDateFormatterShortStyle;
                
                self.arrivalLabel.text = [NSString stringWithFormat:@"%@ arrival", [timeFormatter stringFromDate:arrivalTime]];
                
                self.navDirectionsView.accessibilityLabel = [NSString stringWithFormat:@"%@. %@. %@.", self.arrivalLabel.text, self.distanceLabel.text, self.directionsLabel.text];
                
                //voice announcements
                if (!self.announcedFirstDirection) {
                    self.announcedFirstDirection = YES;
                    [self announceDirectionsForNode:NodeTypeNext];
                }
                
                if (currentLocation.horizontalAccuracy < 15 && currentLocation.horizontalAccuracy > 0) {
                    //if user is close to node, announce which direction to turn
                    if (feet <= 25) {
//                        NSLog(@"within 25 feet of major node");
                        if (self.isApproachingNode == NO) {
//                            NSLog(@"did set approaching node to yes");
                            self.approachingNode = YES;
                            
                            if (self.pathToNavigate.path.count > 1 && [self.nodeToPrompt isEqual:self.pathToNavigate.path[1]]) {
//                                NSLog(@"announcing direction for upcoming node");
                                [self announceDirectionsForNode:NodeTypeNext];
                            }
                        }
                    }
                    //remove node if user has passed it
                    if (feet <= 15) {
//                        NSLog(@"is really close to next node, will remove it after it's passed");
                        self.shouldRemoveUpcomingNode = YES;
                    } else {
                        if (self.shouldRemoveUpcomingNode) {
                            //remove passed node
                            if (self.pathToNavigate.path.count > 1) { //don't remove last node
                                [self announceDirectionsForNode:NodeTypeNext];
                                
//                                NSLog(@"removed the just passed node");
                                [self.pathToNavigate.path removeObjectAtIndex:0];
                                self.approachingNode = NO;
                                self.rightNextToNode = NO;
                            }
                        }
                        
                        self.shouldRemoveUpcomingNode = NO;
                    }
                    
                    if (feet < 10) {
                        if (self.isRightNextToNode == NO) {
//                            NSLog(@"did set right next to node to yes");
                            self.rightNextToNode = YES;
                            
                            if ([self.nodeToPrompt isEqual:self.pathToNavigate.path[1]]) {
                                //if arrived at the destination
                                if (self.pathToNavigate.path.count == 1) {
                                    self.arrivalLabel.text = @"Please deliver order to:";
                                    self.distanceLabel.text = [NSString stringWithFormat:@"%@ %@", self.orderInfo[@"OrderSummaryFirstName"], self.orderInfo[@"OrderSummaryLastName"]];
                                    self.directionsLabel.text = [NSString stringWithFormat:@"Order #%@", self.orderInfo[@"OrderSummaryTicketNumber"]];
                                    
                                    self.supplementaryToolbar.hidden = NO;
                                    self.supplementaryButton.title = @"Mark as Delivered";
                                    [self.supplementaryButton setTarget:self];
                                    [self.supplementaryButton setAction:@selector(didTapDelivered:)];
                                }
                                
//                                NSLog(@"announcing direction for node you're right next to");
                                [self announceDirectionsForNode:NodeTypeImmediate];
                            }
                        }
                    }
                }
            }
        }
    }
}

- (void)locationManagerDidPauseLocationUpdates:(CLLocationManager *)manager {
    //this is called if iOS recognizes the user has stopped walking for a few minutes and screen is turned off - automatically stops updating the user's location
    //if we know they're still navigating we can send a push notification asking if they want to continue navigating because it's automatically started again when the app is returned to the foreground
    //or we can force resume if we are positive they're still walking
    //or we can completely disable this auto pause behavior (highly not recommended - probably performs other battery saving tactics)
}

#pragma mark - UIBarPositionDelegate

- (UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    //set toolbar position to top to ensure border is displayed underneath
    return UIBarPositionTop;
}

#pragma mark - Getters and Setters

- (GMSCameraPosition *)mainCampusCamera {
    if (!_mainCampusCamera) {
        _mainCampusCamera = [GMSCameraPosition cameraWithLatitude:WSU_OGDEN_MAIN_LATITUDE
                                                        longitude:WSU_OGDEN_MAIN_LONGITUDE
                                                             zoom:17];
    }
    return _mainCampusCamera;
}

- (AVSpeechSynthesizer *)speechSynthesizer {
    if (!_speechSynthesizer) {
        _speechSynthesizer = [[AVSpeechSynthesizer alloc] init];
    }
    return _speechSynthesizer;
}

- (void)setDestinationInfo:(NSDictionary *)destinationInfo {
    _destinationInfo = destinationInfo;
    
    if (destinationInfo == nil) {
        self.orderDetailsButton.enabled = NO;
    } else {
        self.orderDetailsButton.enabled = YES;
    }
}

#pragma mark - Methods

- (void)didTapDelivered:(UIBarButtonItem *)sender {
    [self markOrderAsDelivered];
}

- (IBAction)didTapSettings:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"Show Settings" sender:self];
}

- (IBAction)didTapDetail:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"Show Order Details" sender:self];
}

- (IBAction)didTapList:(UIBarButtonItem *)sender {
    [self performSegueWithIdentifier:@"Show Orders" sender:self];
}

- (IBAction)didTapHelp:(UIBarButtonItem *)sender {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:nil
                                                            preferredStyle:UIAlertControllerStyleActionSheet];

    //Log Out
    UIAlertAction *logOut = [UIAlertAction actionWithTitle:@"Log Out"
    style:UIAlertActionStyleDestructive
    handler:^(UIAlertAction *action) {
    [self performSegueWithIdentifier:@"Show Login" sender:self];
    }];
    [alert addAction:logOut];
    
    //Call mentor
    if (self.userInfo) {
        UIAlertAction *callEmployer = [UIAlertAction actionWithTitle:[NSString
                                                                      stringWithFormat:@"Call %@ %@", self.userInfo[@"mentor"][@"firstname"], self.userInfo[@"mentor"][@"lastname"]]
                                                               style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction *action) {
                                                                 NSString *phoneNumber = [@"telprompt://" stringByAppendingString:self.userInfo[@"mentor"][@"phone"]];
                                                                 [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                                                             }];
        [alert addAction:callEmployer];
    }
    
    //Call work
    UIAlertAction *callWork = [UIAlertAction actionWithTitle:@"Call Work"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         NSString *phoneNumber = [@"telprompt://" stringByAppendingString:self.userInfo[@"store"][@"StorePhoneNumber"]];
                                                         [[UIApplication sharedApplication] openURL:[NSURL URLWithString:phoneNumber]];
                                                     }];
    [alert addAction:callWork];
    
    //Go back to work
    UIAlertAction *goBackToWork = [UIAlertAction actionWithTitle:@"Navigate Back to Work"
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         [self stopNavigating];
                                                         self.destinationInfo = self.userInfo[@"store"];
                                                         self.didRequestRoute = NO;
                                                         [self startUpdatingLocation]; //will cause route fetch
                                                     }];
    [alert addAction:goBackToWork];
    
    //Cancel
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil];
    [alert addAction:cancel];
    
    [self presentViewController:alert animated:YES completion:NULL];
    
    [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                      object:nil
                                                       queue:[NSOperationQueue mainQueue]
                                                  usingBlock:^(NSNotification *notification) {
                                                      [alert dismissViewControllerAnimated:NO completion:NULL];
                                                  }];
}

- (void)showLoadingText {
    CGFloat spinnerSize = self.topView.frame.size.height / 2;
    CGFloat verticalOffset = self.topView.frame.size.height / 2 - spinnerSize / 2;
    
    self.navDirectionsView.hidden = YES;
    
    [self.mapView clear]; //remove existing route/markers
    self.pathToNavigate = nil;
    
    [self.loadingLabelView removeFromSuperview];
    self.loadingLabelView = nil;
    
    self.loadingLabelView = [[UIView alloc] initWithFrame:self.topView.bounds];
    
    UILabel *loadingLabel = [[UILabel alloc] init];
    loadingLabel.text = @"Loading route...";
    loadingLabel.font = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    [loadingLabel sizeToFit];
    CGFloat horizontalOffset = (self.topView.frame.size.width - spinnerSize - spinnerSize / 2 - loadingLabel.frame.size.width) /2;
    loadingLabel.frame = CGRectMake(horizontalOffset + spinnerSize, verticalOffset, loadingLabel.frame.size.width, spinnerSize);
    
    UIActivityIndicatorView *navSpinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    [navSpinner startAnimating];
    navSpinner.frame = CGRectMake(horizontalOffset, verticalOffset, spinnerSize, spinnerSize);
    navSpinner.color = [UIColor colorWithWhite:128/255.0 alpha:1.0];
    
    [self.loadingLabelView addSubview:navSpinner];
    [self.loadingLabelView addSubview:loadingLabel];
    
    self.loadingLabelView.alpha = 0;
    [self.topView addSubview:self.loadingLabelView];
    
    [UIView animateWithDuration:0.25 animations:^{
        self.loadingLabelView.alpha = 1;
    } completion:NULL];
    
    UIAccessibilityPostNotification(UIAccessibilityAnnouncementNotification, @"Loading route.");
}

- (void)removeLoadingLabelView {
    [UIView animateWithDuration:0.25 animations:^{
        self.loadingLabelView.alpha = 0;
    } completion:^(BOOL finished) {
        [self.loadingLabelView removeFromSuperview];
        self.loadingLabelView = nil;
    }];
}

- (void)enableContinuousVoicePrompts {
    NSInteger timerInterval = [[NSUserDefaults standardUserDefaults] integerForKey:ContinuousPromptsIntervalKey];
    self.continuousVoicePromptsTimer = [NSTimer scheduledTimerWithTimeInterval:timerInterval
                                                                        target:self
                                                                      selector:@selector(announceContinuousPrompt:)
                                                                      userInfo:nil
                                                                       repeats:YES];
}

- (void)disableContinuousVoicePrompts {
    [self.continuousVoicePromptsTimer invalidate];
    self.continuousVoicePromptsTimer = nil;
}

- (void)startUpdatingLocation {
    [self.locationManager startUpdatingLocation];
    [self.locationManager startUpdatingHeading];
    self.mapView.myLocationEnabled = YES;
}

- (void)startNavigating {
    self.navigationStarted = YES;
    
    [self removeLoadingLabelView];
    self.navDirectionsView.hidden = NO;
    
    if (self.destinationInfo[@"location_data"][@"name"]) {
        [self announceText:[NSString stringWithFormat:@"Starting route to %@.", self.destinationInfo[@"location_data"][@"name"]]];
    } else {
        [self announceText:@"Starting route."];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL continuousPromptsEnabled = [userDefaults boolForKey:ContinuousPromptsEnabledKey];
    if (continuousPromptsEnabled == YES) {
        [self enableContinuousVoicePrompts];
    }
}

- (void)stopNavigating {
    self.navigationStarted = NO;
    [self disableContinuousVoicePrompts];
    [self removeLoadingLabelView];
    [self.locationManager stopUpdatingLocation];
    self.mapView.myLocationEnabled = NO;
    [self.mapView animateToCameraPosition:self.mainCampusCamera];
}

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self updateFontSizes];
}

- (void)updateFontSizes {
    UIFont *subheadFont = [UIFont preferredFontForTextStyle:UIFontTextStyleSubheadline];
    if (subheadFont.pointSize > 21) {
        subheadFont = [subheadFont fontWithSize:21];
    }
    self.arrivalLabel.font = subheadFont;
    
    UIFont *headlineFont = [UIFont preferredFontForTextStyle:UIFontTextStyleHeadline];
    headlineFont = [headlineFont fontWithSize:headlineFont.pointSize * 2];
    if (headlineFont.pointSize > 45) {
        headlineFont = [headlineFont fontWithSize:45];
    }
    self.distanceLabel.font = headlineFont;
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    bodyFont = [bodyFont fontWithSize:bodyFont.pointSize * 1.3];
    if (bodyFont.pointSize > 30) {
        bodyFont = [bodyFont fontWithSize:30];
    }
    self.directionsLabel.font = bodyFont;
}

#pragma mark - Mapping Methods

/** 
 Queries the navigation API to get the shortest path from one GPS coordinate to the destination
 @param coordinate: Starting coordinate (device's current location)
 @param destinationID: Destination ID obtained from navigation API
 */
- (void)getPathWithSource:(CLLocationCoordinate2D)coordinate andDestinationID:(NSString *)destinationID {
    self.didRequestRoute = YES;
    
    //Get the destination and draw route using Klint's API
    NSString *deviceLatitude = [NSString stringWithFormat:@"%.8f", coordinate.latitude];
    NSString *deviceLongitude = [NSString stringWithFormat:@"%.8f", coordinate.longitude];
    
    // build the url for the request
    NSString *string = [NSString stringWithFormat:@"%@path/%@?username=%@&password=%@&lat=%@&lng=%@",
                        NAV_API_URL, destinationID, NAV_API_USER, NAV_API_PASS, deviceLatitude, deviceLongitude];
    NSURL *url = [NSURL URLWithString:string];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    // create the request
    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    operation.responseSerializer = [AFJSONResponseSerializer serializer];
    [operation.securityPolicy setAllowInvalidCertificates:YES]; // needed because the server doesn't have a valid certificate
    
    // execute upon successful request
    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSString *type = [responseObject objectForKey:@"type"];
        if (![type isEqual:@"error"]) {
            self.pathToNavigate = [[NavigatablePath alloc] initWithAPIDictionary:responseObject];
            self.nodeToPrompt = self.pathToNavigate.path.lastObject;
            [self drawRoute:self.pathToNavigate];
            [self startNavigating];
        } else {
            self.didRequestRoute = NO;
            [self stopNavigating];
            
            NSString *message = [responseObject objectForKey:@"message"];
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Load Route"
                                                                           message:message
                                                                    preferredStyle:UIAlertControllerStyleAlert];
            [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                      style:UIAlertActionStyleCancel
                                                    handler:nil]];
            [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                      style:UIAlertActionStyleDefault
                                                    handler:^(UIAlertAction *action) {
                                                        [self showLoadingText];
                                                        [self startUpdatingLocation];
                                                    }]];
            [self presentViewController:alert animated:YES completion:NULL];
            [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                              object:nil
                                                               queue:[NSOperationQueue mainQueue]
                                                          usingBlock:^(NSNotification *notification) {
                                                              [alert dismissViewControllerAnimated:NO completion:NULL];
                                                          }];
        }
    // execute upon failed request
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        self.didRequestRoute = NO;
        [self stopNavigating];
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Load Route"
                                                                       message:[error localizedDescription]
                                                                preferredStyle:UIAlertControllerStyleAlert];
        [alert addAction:[UIAlertAction actionWithTitle:@"OK"
                                                  style:UIAlertActionStyleCancel
                                                handler:nil]];
        [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                  style:UIAlertActionStyleDefault
                                                handler:^(UIAlertAction *action) {
            [self showLoadingText];
            [self startUpdatingLocation];
        }]];
        [self presentViewController:alert animated:YES completion:NULL];
        [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                          object:nil
                                                           queue:[NSOperationQueue mainQueue]
                                                      usingBlock:^(NSNotification *notification) {
            [alert dismissViewControllerAnimated:NO completion:NULL];
        }];
    }];
    
    // start the request
    [operation start];
}

- (void)markOrderAsDelivered {
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSString *orderSummaryID = [NSString stringWithFormat:@"%@", self.orderInfo[@"order_details"][@"OrderSummaryID"]];
    NSDictionary *parameters = @{ @"api_username": ORDERS_API_USER,
                                  @"api_pass": ORDERS_API_PASS,
                                  @"order_summary_id": orderSummaryID };
    [manager POST:[ORDERS_API_URL stringByAppendingString:ORDERS_API_MARK_ORDER_DELIVERED]
       parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
           //order successfully marked as delivered
           //navigate back to work
           self.supplementaryToolbar.hidden = YES;
           [self stopNavigating];
           self.destinationInfo = self.userInfo[@"store"];
           self.didRequestRoute = NO;
           [self startUpdatingLocation]; //will cause route fetch
       } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
           //failed to mark as delivered
           UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Unable to Mark Order Delivered"
                                                                          message:@"Please try again."
                                                                   preferredStyle:UIAlertControllerStyleAlert];
           [alert addAction:[UIAlertAction actionWithTitle:@"Cancel"
                                                     style:UIAlertActionStyleCancel
                                                   handler:nil]];
           [alert addAction:[UIAlertAction actionWithTitle:@"Retry"
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                                       [self markOrderAsDelivered];
                                                   }]];
           [self presentViewController:alert animated:YES completion:NULL];
           [[NSNotificationCenter defaultCenter] addObserverForName:UIApplicationWillResignActiveNotification
                                                             object:nil
                                                              queue:[NSOperationQueue mainQueue]
                                                         usingBlock:^(NSNotification *notification) {
                                                             [alert dismissViewControllerAnimated:NO completion:NULL];
                                                         }];
       }];
}

- (void)drawRoute:(NavigatablePath *)navPath {
    NSMutableArray *pathList = [[NSMutableArray alloc] init];
    pathList = navPath.path;
    GMSMutablePath *path = [GMSMutablePath path];
    
    for (int i = 0; i < [pathList count]; i++) {
        [path addLatitude:[(NavigationNode *)pathList[i] latitude] longitude:[(NavigationNode *)pathList[i]longitude]];
        if (i == [pathList count] - 1) {
            // place marker on destination
            GMSMarker *marker = [[GMSMarker alloc] init];
            marker.position = CLLocationCoordinate2DMake([(NavigationNode *)pathList[i] latitude], [(NavigationNode *)pathList[i] longitude]);
            marker.title = ((NavigationNode *)pathList[i]).name;
            marker.map = self.mapView;
        }
    }
    
    GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
    polyline.strokeColor = self.view.tintColor;
    polyline.strokeWidth = 4.0;
    polyline.map = self.mapView;
}

- (void)announceContinuousPrompt:(NSTimer *)timer {
    if (self.navigationStarted) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        if ([userDefaults boolForKey:ContinuousPromptsEnabledKey]) {
            [self announceDirectionsForNode:NodeTypeNext];
        } else {
            [self.continuousVoicePromptsTimer invalidate];
            self.continuousVoicePromptsTimer = nil;
        }
    }
}

- (void)announceDirectionsForNode:(NodeType)nodeType {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL voiceEnabled = [userDefaults boolForKey:VoiceCommandsEnabledKey];
    
    if (voiceEnabled) {
        NSString *stringToAnnounce = nil;
        
        if (nodeType == NodeTypeNext) {
            //if the upcoming node is the destination
            if (self.pathToNavigate.path.count == 1) {
                stringToAnnounce = [NSString stringWithFormat:@"In %@, you will reach your destination. %@", self.distanceLabel.text, self.directionsLabel.text];
            } else {
                stringToAnnounce = [NSString stringWithFormat:@"In %@, %@.", self.distanceLabel.text, self.directionsLabel.text];
            }
        } else if (nodeType == NodeTypeImmediate) {
            //if destination
            if (self.pathToNavigate.path.count == 1) {
                stringToAnnounce = [NSString stringWithFormat:@"You have arrived at your destination. %@ %@ %@.", self.arrivalLabel.text, self.distanceLabel.text, self.directionsLabel.text];
            } else {
                stringToAnnounce = [NSString stringWithFormat:@"%@.", self.directionsLabel.text];
            }
        }
        
        [self announceText:stringToAnnounce];
    }
}

- (void)announceText:(NSString *)text {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:text];
    utterance.rate = [userDefaults floatForKey:VoiceCommandsRateKey];
    utterance.pitchMultiplier = [userDefaults floatForKey:VoiceCommandsPitchKey];
    [self.speechSynthesizer speakUtterance:utterance];
}

/**
 Calculates the direction the user needs to turn at the next major node
 @param path: Array of nodes along the path
 @return -1: error occurred
 @return 0: turn left
 @return 1: slight left
 @return 2: slight right
 @return 3: turn right
 @return 4: continue straight
 */
- (int)calculateTurnDirection:(NSMutableArray *)path {
    if (path.count >= 3) {
        double bearing2and1;
        double bearing3and2;
        int i = 0;
        
        // get the first set of nodes
        NavigationNode *node1 = path[0];
        NavigationNode *node3;
        
        do {
            // get the node that will be used for calculating bearing differences
            self.nodeToPrompt = path[1 + i];
            
            node3 = path[2 + i];
            
            //calculate feet until next node
            CLLocationCoordinate2D node1Coordinate = CLLocationCoordinate2DMake(node1.latitude, node1.longitude);
            CLLocationCoordinate2D node2Coordinate = CLLocationCoordinate2DMake(self.nodeToPrompt.latitude, self.nodeToPrompt.longitude);
            CLLocationDistance meters = GMSGeometryDistance(node1Coordinate, node2Coordinate);
            CLLocationDistance feet = meters * 3.2808;
            
            // prompt the user to continue straight if the distance to the node is greater than 500ft
            if (feet > 500) {
                return 4;
            }

            // Calculate the differences in latitude and longitude between the 3 nodes
            double latDiff2and1 = self.nodeToPrompt.latitude - node1.latitude;
            double longDiff2and1 = self.nodeToPrompt.longitude - node1.longitude;
            double latDiff3and2 = node3.latitude - self.nodeToPrompt.latitude;
            double longDiff3and2 = node3.longitude - self.nodeToPrompt.longitude;

            // calculate the bearings, these will be between -1.5708 and 1.5708
            double atan2and1 = atan(latDiff2and1/longDiff2and1);
            double atan3and2 = atan(latDiff3and2/longDiff3and2);
            
            // calculate the bearings of the two vectors in decimal between -90 and 90
            bearing2and1 = [self convertRadiansToDecimal:atan2and1];
            bearing3and2 = [self convertRadiansToDecimal:atan3and2];
            
            // adjust the arctan by 180 degrees if the bearing is in quadrant 2 or 3, end result should be a bearing between 0 and 360
            if (longDiff2and1 < 0) {
                bearing2and1 += 180;
            }
            if (longDiff3and2 < 0) {
                bearing3and2 += 180;
            }
            
            i++;
        } while (fabs(bearing2and1 - bearing3and2) < 10 && path.count >= 2 + i); // only calculate turn directions when the user is required to turn more than 10 degrees

        // calculate the turn
        double bearingDiff = bearing2and1 - bearing3and2;
        
        //Return Data
        // turn left
        if ((-180 <= bearingDiff && bearingDiff < -30) || (180 <= bearingDiff && bearingDiff < 360)) {
            return 0;
        }
        // slight left
        else if (-30 <= bearingDiff && bearingDiff < 0) {
            return 1;
        }
        // slight right
        else if (0 <= bearingDiff && bearingDiff < 30) {
            return 2;
        }
        // turn right
        else if ((-360 <= bearingDiff && bearingDiff < -180) || (30 <= bearingDiff && bearingDiff < 180)) {
            return 3;
        }
        // there is an error in the code
        else {
            return -1;
        }
    }
    
    return 4; //if no turns in path go straight
}

// A method to convert a radian value to a decimal value between 0 and 360
/**
 Convert a radian value to decimal
 @param radValue: The radian value to convert
 @return The decimal value equivalent, between 0 and 360
 */
- (double)convertRadiansToDecimal:(double)radValue {
    double decValue = radValue * 180 / M_PI;
    return decValue;
}

#pragma mark - App Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"Show Order Details"]) {
        UINavigationController *navController = (UINavigationController *)segue.destinationViewController;
        OrderDetailsTableViewController *orderDetailsTVC = (OrderDetailsTableViewController *)navController.viewControllers[0];
        orderDetailsTVC.orderDetails = self.orderInfo;
    }
}

- (IBAction)unwindFromSettings:(UIStoryboardSegue *)segue {
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (IBAction)unwindFromOrders:(UIStoryboardSegue *)segue {
    [self dismissViewControllerAnimated:YES completion:NULL];
    
    [self showLoadingText];
    
    self.navigationStarted = NO;
    self.didRequestRoute = NO;
    
    if (!self.locationManager) {
        self.locationManager = [[CLLocationManager alloc] init];
        self.locationManager.delegate = self;
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
        self.locationManager.activityType = CLActivityTypeFitness; //pedestrian activities
    }
    
    if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
        [self startUpdatingLocation];
    } else {
        [self.locationManager requestWhenInUseAuthorization]; //calls didChangeAuthorizationStatus
    }
}

@end
