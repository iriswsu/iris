//
//  OrderDetailsTableViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 2/1/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

/**
 Table view controller that shows the detail information for a given order
 */
@interface OrderDetailsTableViewController : UITableViewController

@property (nonatomic, strong) NSDictionary *orderDetails;

@end
