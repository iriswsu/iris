//
//  NavViewController.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/18/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

/**
 The navigation view controller - manages all location monitoring, routing, and directions
 */
@interface NavViewController : UIViewController <CLLocationManagerDelegate, UIBarPositioningDelegate>

extern NSString *const VoiceCommandsEnabledKey;
extern NSString *const VoiceCommandsPitchKey;
extern NSString *const VoiceCommandsRateKey;
extern NSString *const ContinuousPromptsEnabledKey;
extern NSString *const ContinuousPromptsIntervalKey;

@property (strong, nonatomic) NSDictionary *userInfo; //information about the logged in user
@property (strong, nonatomic) NSDictionary *orderInfo; //information about the order being delivered
@property (strong, nonatomic) NSDictionary *destinationInfo; //information about the destination the order is being delivered to

@end
