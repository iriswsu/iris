//
//  AppDelegate.h
//  IRIS
//
//  Created by Jordan Hipwell on 1/14/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

