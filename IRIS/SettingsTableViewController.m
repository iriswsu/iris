//
//  SettingsTableViewController.m
//  IRIS
//
//  Created by Jordan Hipwell on 3/23/15.
//  Copyright (c) 2015 Weber State University. All rights reserved.
//

#import "SettingsTableViewController.h"
#import "NavViewController.h"
#import <AVFoundation/AVFoundation.h>

@interface SettingsTableViewController ()

@property (weak, nonatomic) IBOutlet UITableViewCell *voiceCommandsCell;
@property (strong, nonatomic) UISwitch *voiceCommandsSwitch;
@property (weak, nonatomic) IBOutlet UISlider *voicePitchSlider;
@property (weak, nonatomic) IBOutlet UISlider *voiceRateSlider;
@property (weak, nonatomic) IBOutlet UITableViewCell *continuousPromptsCell;
@property (strong, nonatomic) UISwitch *continuousPromptsSwitch;
@property (weak, nonatomic) IBOutlet UITableViewCell *announcementIntervalCell;

@end

@implementation SettingsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        self.view.backgroundColor = [UIColor clearColor];
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        blurEffectView.frame = self.view.frame;
        self.tableView.backgroundView = blurEffectView;
        
        self.tableView.separatorEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
    }
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL voiceCommandsEnabled = [userDefaults boolForKey:VoiceCommandsEnabledKey];
    BOOL continuousPromptsEnabled = [userDefaults boolForKey:ContinuousPromptsEnabledKey];
    
    UISwitch *voiceCommandsSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    voiceCommandsSwitch.on = voiceCommandsEnabled;
    voiceCommandsSwitch.onTintColor = [[[UIApplication sharedApplication] delegate] window].tintColor;
    [voiceCommandsSwitch addTarget:self action:@selector(voiceCommandsToggled:) forControlEvents:UIControlEventValueChanged];
    self.voiceCommandsSwitch = voiceCommandsSwitch;
    self.voiceCommandsCell.accessoryView = voiceCommandsSwitch;
    
    UISwitch *continuousPromptsSwitch = [[UISwitch alloc] initWithFrame:CGRectZero];
    continuousPromptsSwitch.on = continuousPromptsEnabled;
    continuousPromptsSwitch.onTintColor = [[[UIApplication sharedApplication] delegate] window].tintColor;
    [continuousPromptsSwitch addTarget:self action:@selector(continuousPromptsToggled:) forControlEvents:UIControlEventValueChanged];
    self.continuousPromptsSwitch = continuousPromptsSwitch;
    self.continuousPromptsCell.accessoryView = continuousPromptsSwitch;
    
    self.voicePitchSlider.minimumValue = 0.5;
    self.voicePitchSlider.maximumValue = 2.0 / 1.15;
    self.voicePitchSlider.value = [userDefaults floatForKey:VoiceCommandsPitchKey];
    
    self.voiceRateSlider.minimumValue = AVSpeechUtteranceMinimumSpeechRate;
    self.voiceRateSlider.maximumValue = AVSpeechUtteranceMaximumSpeechRate / 2;
    self.voiceRateSlider.value = [userDefaults floatForKey:VoiceCommandsRateKey];
    
    self.announcementIntervalCell.accessibilityTraits = UIAccessibilityTraitButton;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preferredFontsChanged:)
                                                 name:UIContentSizeCategoryDidChangeNotification
                                               object:nil];
    
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    
    NSInteger interval = [[NSUserDefaults standardUserDefaults] integerForKey:ContinuousPromptsIntervalKey];
    
    self.announcementIntervalCell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", (long)interval];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIContentSizeCategoryDidChangeNotification
                                                  object:nil];
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == 1 || section == 2) {
        if (!self.voiceCommandsSwitch.on) {
            return nil;
        } else {
            if (section == 1) {
                return @"Voice Pitch";
            } else if (section == 2) {
                return @"Speaking Rate";
            }
        }
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 1 || section == 2 || section == 3) {
        if (!self.voiceCommandsSwitch.on) {
            return 0;
        }
    }
    if (section == 3) {
        if (!self.continuousPromptsSwitch.on) {
            return 1;
        }
    }
    
    if (section == 0 || section == 1 || section == 2) {
        return 1;
    } else if (section == 3) {
        return 2;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat cellHeight = 60;
    
    NSString *preferredSize = [[UIApplication sharedApplication] preferredContentSizeCategory];
    float offset = 1;
    
    if ([preferredSize isEqualToString:UIContentSizeCategoryExtraSmall]) {
        offset = 0.9;
    } else if ([preferredSize isEqualToString:UIContentSizeCategorySmall]) {
        offset = 0.95;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryMedium]) {
        offset = 1.0;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryLarge]) {
        offset = 1.1;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraLarge]) {
        offset = 1.2;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraLarge]) {
        offset = 1.3;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryExtraExtraExtraLarge]) {
        offset = 1.4;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityMedium]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraLarge]) {
        offset = 1.5;
    } else if ([preferredSize isEqualToString:UIContentSizeCategoryAccessibilityExtraExtraExtraLarge]) {
        offset = 1.5;
    }
    
    cellHeight = cellHeight * offset;
    
    return cellHeight;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        cell.backgroundColor = [[UIColor whiteColor] colorWithAlphaComponent:0.4];
        
        //add vibrant effect for cell highlight
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVibrancyEffect *vibrantEffect = [UIVibrancyEffect effectForBlurEffect:blurEffect];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:vibrantEffect];
        visualEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        UIView *coloredView = [[UIView alloc] init];
        coloredView.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.5];
        coloredView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [visualEffectView.contentView addSubview:coloredView];
        cell.selectedBackgroundView = visualEffectView;
    } else {
        UIView *selectedBackgroundView = [[UIView alloc] init];
        selectedBackgroundView.backgroundColor = [UIColor colorWithWhite:235/255.0 alpha:1.0];
        cell.selectedBackgroundView = selectedBackgroundView;
    }
    
    UIFont *bodyFont = [UIFont preferredFontForTextStyle:UIFontTextStyleBody];
    if (bodyFont.pointSize > 26) {
        bodyFont = [bodyFont fontWithSize:26];
    }
    cell.textLabel.font = bodyFont;
    cell.detailTextLabel.font = [bodyFont fontWithSize:bodyFont.pointSize / 1.15];
    
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.detailTextLabel.backgroundColor = [UIColor clearColor];
}

#pragma mark - Methods

- (void)voiceCommandsToggled:(UISwitch *)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:sender.on forKey:VoiceCommandsEnabledKey];
    [userDefaults synchronize];
    
    [self.tableView reloadData];
}

- (void)continuousPromptsToggled:(UISwitch *)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setBool:sender.on forKey:ContinuousPromptsEnabledKey];
    [userDefaults synchronize];
    
    [self.tableView reloadData];
}

- (IBAction)pitchChanged:(UISlider *)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:sender.value forKey:VoiceCommandsPitchKey];
    
    [self speakDemoText];
}

- (IBAction)rateChanged:(UISlider *)sender {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setFloat:sender.value forKey:VoiceCommandsRateKey];
    
    [self speakDemoText];
}

- (void)preferredFontsChanged:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)speakDemoText {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:@"Hello, I will route you to your destination."];
    utterance.rate = [userDefaults floatForKey:VoiceCommandsRateKey];
    utterance.pitchMultiplier = [userDefaults floatForKey:VoiceCommandsPitchKey];
    [[[AVSpeechSynthesizer alloc] init] speakUtterance:utterance];
}

@end
